# puppetlab-docker

Docker Puppet laboratory

# Requirements

* [Docker](https://docs.docker.com/get-docker/)

* [Docker Compose](https://docs.docker.com/compose/install/)

# Overview
This project has some machines to model this architecture:

![alt text](img/Puppet_Architecture_Black.png "Architecture")

# How to start
```bash
x11tete11x@warmachine-tt1:~/GIT/puppetlab$ docker-compose up --build -d
...
Creating puppetlab_puppet_1 ... done
Creating puppetlab_puppet-client-tt2_1 ... done
Creating puppetlab_puppet-client-tt1_1 ... done
Creating puppetlab_puppetdb_1          ... done
```

# How to enter into a machine
```bash
x11tete11x@warmachine-tt1:~/GIT/puppetlab$ docker exec -ti puppetlab_puppetdb_1 bash
```

For each machine make sure that you see __STARTUP DONE!__ in startup logs, this means that the machine it's ready to be used, otherwise it's still auto configure itself

```bash
root@puppetdb:/# tail -f /var/log/supervisor/init-std*
...
STARTUP DONE!

```

# Timeline
![alt text](img/Puppet_Laboratory_Timeline_Black.png "Timeline")