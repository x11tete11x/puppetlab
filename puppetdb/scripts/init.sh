#!/bin/bash
/opt/scripts/dump-env.sh
/opt/scripts/add-service.sh $SERVICE_PUPPET_NAME puppet $DOMAIN
PUPPETDB_IP=$(/opt/puppetlabs/bin/facter networking.ip)
/opt/scripts/run-script-in-puppet.sh add-puppetdb.sh $PUPPETDB_IP $DOMAIN

/opt/scripts/configure-role.sh

/opt/scripts/wait-for-puppet-connectivity.sh

/opt/scripts/init-puppet-agent.sh

/opt/scripts/wait-for-puppet-agent.sh

/opt/puppetlabs/bin/puppet agent --test

scp -v -i /root/.ssh/puppet-ssh /etc/puppetlabs/puppetdb/ssl/private.pem puppet.$DOMAIN:/etc/ssl/private/puppetdb/
scp -v -i /root/.ssh/puppet-ssh /etc/puppetlabs/puppetdb/ssl/public.pem /etc/puppetlabs/puppetdb/ssl/private.pem puppet.$DOMAIN:/etc/ssl/private/puppetdb/
/opt/scripts/run-script-in-puppet.sh configure-puppetdb.sh puppetdb.$DOMAIN 8081
echo "STARTUP DONE!"
