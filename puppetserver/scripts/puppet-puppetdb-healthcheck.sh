#!/bin/bash
while true
do
  if nc -z puppetdb.$DOMAIN 8081
  then
    if ( ! grep -q 'storeconfigs = true' /etc/puppetlabs/puppet/puppet.conf ) and ( ! grep -q 'storeconfigs_backend = puppetdb' /etc/puppetlabs/puppet/puppet.conf )
    then
      echo 'storeconfigs = true' >> /etc/puppetlabs/puppet/puppet.conf
      echo 'storeconfigs_backend = puppetdb' >> /etc/puppetlabs/puppet/puppet.conf
      service puppetserver stop
      sleep 1
      service puppetserver start
    fi
    /opt/scripts/set-status-puppet-healtcheck.sh "OK"
  else
    /opt/scripts/set-status-puppet-healtcheck.sh "FAIL"
    if ( grep -q 'storeconfigs = true' /etc/puppetlabs/puppet/puppet.conf ) and ( grep -q 'storeconfigs_backend = puppetdb' /etc/puppetlabs/puppet/puppet.conf )
    then
      echo 'storeconfigs = true' >> /etc/puppetlabs/puppet/puppet.conf
      echo 'storeconfigs_backend = puppetdb' >> /etc/puppetlabs/puppet/puppet.conf
      service puppetserver stop
      sleep 1
      service puppetserver start
    fi
  fi
  sleep 5
done
