#!/bin/bash
if [ ! -d "/etc/puppetlabs/code/environments/production" ]
then
  git clone "$GIT_PUPPETMASTER_URL" /etc/puppetlabs/code/environments/production
  pushd . >/dev/null
  cd /etc/puppetlabs/code/environments/production
  git checkout "$GIT_PUPPETMASTER_BRANCH"
  /opt/puppetlabs/puppet/bin/r10k puppetfile install Puppetfile
  popd >/dev/null
else
  pushd . >/dev/null
  cd /etc/puppetlabs/code/environments/production
  /opt/puppetlabs/puppet/bin/r10k puppetfile install Puppetfile
  popd >/dev/null
fi
