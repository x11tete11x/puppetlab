#!/bin/bash
/opt/scripts/dump-env.sh
/opt/scripts/configure-puppet-puppetdb-healthcheck.sh
echo "127.0.0.1 puppet puppet.$DOMAIN" >> /etc/hosts
sed '/JAVA_ARGS=/d' -i /etc/default/puppetserver
echo 'JAVA_ARGS="-Xms512m -Xmx512m -Djruby.logger.class=com.puppetlabs.jruby_utils.jruby.Slf4jLogger"' >> /etc/default/puppetserver
mkdir -p /etc/ssl/certs/puppetdb
mkdir -p /etc/ssl/private/puppetdb
/opt/puppetlabs/bin/puppetserver ca setup
/opt/scripts/init-code.sh
service puppetserver start
echo "STARTUP DONE!"
