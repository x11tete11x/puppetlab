#!/bin/bash
until nc -z $1 $2; do
  >&2 echo "PuppetDB is unavailable - sleeping"
  sleep 5
done

cat << EOF > /etc/puppetlabs/puppet/puppetdb.conf
[main]
server_urls = https://$1:$2
EOF
/opt/puppetlabs/bin/puppet resource package puppetdb-termini ensure=latest
sed '/storeconfigs/d' -i /etc/default/puppetserver
sed '/storeconfigs_backend/d' -i /etc/default/puppetserver
echo 'storeconfigs = true' >> /etc/puppetlabs/puppet/puppet.conf
echo 'storeconfigs_backend = puppetdb' >> /etc/puppetlabs/puppet/puppet.conf
service puppetserver stop
sleep 1
service puppetserver start
/opt/scripts/set-status-puppet-healtcheck.sh "OK"
