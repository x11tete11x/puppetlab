#!/bin/bash
/opt/scripts/run-script-in-puppet.sh clean-node.sh $(hostname -f) || true
rm -rf /etc/puppetlabs/puppet/ssl
nohup /opt/puppetlabs/bin/puppet agent --test --certname $(hostname -f) --server puppet.$DOMAIN --waitforcert 30 > /var/log/puppet-agent-init-run.log &
sleep 10
/opt/scripts/run-script-in-puppet.sh add-node.sh $(hostname -f)
sleep 5
