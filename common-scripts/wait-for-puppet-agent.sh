#/bin/bash
until ! pgrep puppet > /dev/null; do
  >&2 echo "Puppet agent running - sleeping"
  sleep 10
done
