#!/bin/bash
cat << EOF > /etc/puppetlabs/puppet/csr_attributes.yaml
---
extension_requests:
  pp_role: "$ROLE"
EOF
