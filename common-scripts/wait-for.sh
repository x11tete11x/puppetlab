#/bin/bash
until ${@:2}; do
  >&2 echo "$1"
  sleep 10
done
