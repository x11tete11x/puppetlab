#!/bin/bash
sed "/$2/d" -i /etc/hosts
IP="$(dig +short $1)"
echo "$IP $2 $2.$3" >> /etc/hosts
