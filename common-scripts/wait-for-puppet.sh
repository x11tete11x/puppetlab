#/bin/bash
until curl -sq http://puppet.$DOMAIN/healthcheck | grep -qw OK; do
  >&2 echo "Puppet healtcheck fail - sleeping"
  sleep 10
done
