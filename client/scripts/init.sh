#!/bin/bash
/opt/scripts/dump-env.sh
/opt/scripts/add-service.sh $SERVICE_PUPPET_NAME puppet $DOMAIN

/opt/scripts/configure-role.sh

/opt/scripts/wait-for-puppet-connectivity.sh

/opt/scripts/wait-for-puppet.sh

/opt/scripts/init-puppet-agent.sh

/opt/scripts/wait-for-puppet-agent.sh

/opt/puppetlabs/bin/puppet agent --test
echo "STARTUP DONE!"
